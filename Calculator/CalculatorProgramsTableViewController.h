//
//  CalculatorProgramsTableViewController.h
//  Calculator
//
//  Created by Maik Brendler on 24.07.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CalculatorProgramsTableViewController;

@protocol CalculatorProgramsTableViewControllerDelegate <NSObject>

@optional
- (void)calculatorProgramsTableViewController:(CalculatorProgramsTableViewController*)sender
                                 choseProgram:(id)program;

@end

@interface CalculatorProgramsTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *programs; // Array of CalculatorBrain programs
@property (nonatomic, weak) id<CalculatorProgramsTableViewControllerDelegate>delegate;
@end
