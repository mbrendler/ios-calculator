//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

- (void)pushOperand:(double)operand;
- (void)pushVariable:(NSString*)name;
- (void)pushOperation:(NSString *)operation;
- (id)pop;
- (void)clear;

// program is allways be guarantied to be a property list
@property (readonly) id program;

+ (double)runProgram:(id)program
 usingVariableValues:(NSDictionary *)variableValues;
+ (NSString*)descriptionOfProgram:(id)program;
+ (NSSet *)variablesUsedInProgram:(id)program;

@end
