//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"
#import "GraphViewController.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;
@property (nonatomic, strong) NSDictionary* testVariableValues;
@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize log = _log;
@synthesize variables = _variables;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;
@synthesize testVariableValues = _testVariableValues;

- (CalculatorBrain*)brain {
    if (!_brain) _brain = [[CalculatorBrain alloc] init];
    return _brain;
}

- (IBAction)digitPressed:(UIButton*)sender {
    NSString *digit = sender.currentTitle;
    if (self.userIsInTheMiddleOfEnteringANumber) {
        self.display.text = [self.display.text stringByAppendingString:digit];
    } else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

- (IBAction)pointPressed {
    NSRange rangeOfPoint = [self.display.text rangeOfString:@"."];
    if (self.userIsInTheMiddleOfEnteringANumber) {
        if (rangeOfPoint.location == NSNotFound) {
            self.display.text = [self.display.text stringByAppendingFormat:@"."];
        }
    } else {
        self.display.text = @"0.";
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

- (IBAction)backspacePressed {
    if (self.userIsInTheMiddleOfEnteringANumber) {
        NSUInteger newLength = [self.display.text length] - 1;
        if (newLength) {
            self.display.text = [self.display.text substringToIndex:newLength];
        } else {
            self.display.text = @"0";
            self.userIsInTheMiddleOfEnteringANumber = NO;
        }
    }
}

- (void)logInteractionWithBrain {
    id program = self.brain.program;
    self.log.text = [CalculatorBrain descriptionOfProgram:program];
    NSString *variables = @"";
    for (NSString *variable in [CalculatorBrain variablesUsedInProgram:program]) {
        id value = [self.testVariableValues objectForKey:variable];
        if (!value) value = @"0";
        variables = [variables stringByAppendingFormat:@"  %@ = %@",
                     variable, value];
    }
    self.variables.text = variables;
}

- (IBAction)enterPressed {
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfEnteringANumber = NO;
    [self logInteractionWithBrain];
}

- (void)calculate {
    double result = [CalculatorBrain runProgram:self.brain.program
                            usingVariableValues:self.testVariableValues];
    self.display.text = [NSString stringWithFormat:@"%g", result];
    [self logInteractionWithBrain];
}

- (IBAction)operationPressed:(UIButton*)sender {
    if (self.userIsInTheMiddleOfEnteringANumber) {
        [self enterPressed];
    }
    [self.brain pushOperation:sender.currentTitle];
    [self calculate];
}

- (IBAction)plusMinusPressed:(UIButton*)sender {
    if (self.userIsInTheMiddleOfEnteringANumber) {
        if([self.display.text hasPrefix:@"-"]) {
            self.display.text = [self.display.text substringFromIndex:1];
        } else {
            self.display.text = [@"-" stringByAppendingString:self.display.text];
        }
    }
    else {
        [self operationPressed:sender];
    }
}

- (IBAction)clearPressed {
    [self.brain clear];
    [self calculate];
}

- (IBAction)variablePressed:(UIButton*)sender {
    [self.brain pushVariable:sender.currentTitle];
    [self logInteractionWithBrain];
}

- (void)viewDidUnload {
    [self setVariables:nil];
    [super viewDidUnload];
}

- (IBAction)undoPressed {
    if (self.userIsInTheMiddleOfEnteringANumber) {
        [self backspacePressed];
    } else {
        [self.brain pop];
    }
    [self calculate];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowGraph"]) {
        [segue.destinationViewController setProgram:self.brain.program];
    }
}

- (IBAction)updateGraph
{
    GraphViewController *graph = [self.splitViewController.viewControllers lastObject];
    graph.program = self.brain.program;
}

@end
