//
//  main.m
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
