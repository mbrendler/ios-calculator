//
//  GraphViewControllerViewController.h
//  Calculator
//
//  Created by Maik Brendler on 13.07.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitViewBarButtonItemPresenter.h"

@interface GraphViewController : UIViewController <SplitViewBarButtonItemPresenter>

@property (nonatomic, strong) NSArray *program;

@end
