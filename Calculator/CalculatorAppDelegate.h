//
//  CalculatorAppDelegate.h
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
