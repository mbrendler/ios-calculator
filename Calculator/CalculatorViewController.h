//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RotatableViewController.h"

@interface CalculatorViewController : RotatableViewController

@property (weak, nonatomic) IBOutlet UILabel *display;

@property (weak, nonatomic) IBOutlet UILabel *log;

@property (weak, nonatomic) IBOutlet UILabel *variables;

@end
