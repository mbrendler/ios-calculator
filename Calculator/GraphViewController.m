//
//  GraphViewControllerViewController.m
//  Calculator
//
//  Created by Maik Brendler on 13.07.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import "GraphViewController.h"
#import "GraphView.h"
#import "CalculatorBrain.h"
#import "CalculatorProgramsTableViewController.h"

@interface GraphViewController () <GraphViewDataSource, CalculatorProgramsTableViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *functionDisplay;
@property (weak, nonatomic) IBOutlet GraphView *graphView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@end

@implementation GraphViewController
@synthesize functionDisplay = _functionDisplay;
@synthesize graphView = _graphView;
@synthesize program = _program;
@synthesize splitViewBarButtonItem = _splitViewBarButtonItem;
@synthesize toolbar = _toolbar;

#define FAVORITES_KEY @"GraphViewController.Favorites"

- (IBAction)addtoFavorites:(id)sender
{
    if (!self.program) return;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *favorietes = [[defaults objectForKey:FAVORITES_KEY] mutableCopy];
    if (!favorietes) favorietes = [NSMutableArray array];
    [favorietes addObject:self.program];
    [defaults setObject:favorietes forKey:FAVORITES_KEY];
    [defaults synchronize];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"Show Favorite Graphs"]) {
        NSArray *programs = [[NSUserDefaults standardUserDefaults] objectForKey:FAVORITES_KEY];
        [segue.destinationViewController setPrograms:programs];
        [segue.destinationViewController setDelegate:self];
    }
}

- (NSString*)descriptionOfProgram
{
    if ([self.functionDisplay.text isEqualToString:@""]) {
        return [CalculatorBrain descriptionOfProgram:self.program];
    }
    return nil;
}

- (void)setProgram:(NSArray *)program
{
    _program = program;
    [self.graphView setNeedsDisplay];
    NSString *programDescription = [CalculatorBrain descriptionOfProgram:program];
    if (programDescription) self.functionDisplay.text = programDescription;
}

- (void)handleSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem) [toolbarItems removeObject:_splitViewBarButtonItem];
    if (splitViewBarButtonItem) [toolbarItems insertObject:splitViewBarButtonItem atIndex:0];
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = splitViewBarButtonItem;
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    if (splitViewBarButtonItem != _splitViewBarButtonItem) {
        [self handleSplitViewBarButtonItem:splitViewBarButtonItem];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self handleSplitViewBarButtonItem:self.splitViewBarButtonItem];
    NSString *programDescription = [self descriptionOfProgram];
    if (programDescription) self.functionDisplay.text = programDescription;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *programDescription = [self descriptionOfProgram];
    if (!programDescription) return;
    NSDictionary *viewSettings = [[NSUserDefaults standardUserDefaults]
                                  objectForKey:programDescription];
    if (viewSettings) {
        self.graphView.scale = [[viewSettings objectForKey:@"scale"] doubleValue];
        self.graphView.origin = CGPointMake(
                        [[viewSettings objectForKey:@"origin.x"] doubleValue],
                        [[viewSettings objectForKey:@"origin.y"] doubleValue]);
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    NSString *programDescription = [self descriptionOfProgram];
    if (!programDescription) return;
    [super viewWillDisappear:animated];
    NSDictionary *viewSettings = [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithDouble:self.graphView.scale], @"scale",
            [NSNumber numberWithDouble:self.graphView.origin.x], @"origin.x",
            [NSNumber numberWithDouble:self.graphView.origin.y], @"origin.y",
            nil];
    [[NSUserDefaults standardUserDefaults]
     setObject:viewSettings
        forKey:programDescription];
}

- (double)ordinateByGraphView:(GraphView*)view andAbscissae:(double)x
{
    NSDictionary *variables = [NSDictionary
                    dictionaryWithObject:[NSNumber numberWithDouble:x] 
                                  forKey:@"x"];
    return [CalculatorBrain runProgram:self.program
                   usingVariableValues:variables];
}

- (void)setGraphView:(GraphView*)graphView
{
    _graphView = graphView;
    [self.graphView addGestureRecognizer:[[UIPinchGestureRecognizer alloc]initWithTarget:self.graphView action:@selector(pinch:)]];
    [self.graphView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self.graphView action:@selector(pan:)]];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self.graphView action:@selector(tap:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.graphView addGestureRecognizer:tapGesture];
    self.graphView.dataSource = self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidUnload
{
    [self setFunctionDisplay:nil];
    [self setGraphView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)calculatorProgramsTableViewController:(CalculatorProgramsTableViewController*)sender
                                 choseProgram:(id)program
{
    self.program = program;
}
@end
