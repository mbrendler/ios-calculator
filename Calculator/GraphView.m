//
//  GraphView.m
//  Calculator
//
//  Created by Maik Brendler on 13.07.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import "GraphView.h"
#import "AxesDrawer.h"

@interface GraphView()
@property (nonatomic) CGPoint middle;
@end

@implementation GraphView
@synthesize dataSource = _dataSource;
@synthesize scale = _scale;
@synthesize origin = _origin;

#define DEFAULT_SCALE 20.0

- (double)scale
{
    if (_scale > 0) {
        return _scale;
    }
    return DEFAULT_SCALE;
}

- (void)setScale:(double)scale
{
    if (scale != _scale) {
        _scale = scale;
        [self setNeedsDisplay];
    }
}

- (void)setOrigin:(CGPoint)origin
{
    if (origin.x != _origin.x || origin.y != _origin.y) {
        _origin = origin;
        [self setNeedsDisplay];
    }
}

- (CGPoint)middle
{
    return CGPointMake(
        self.bounds.origin.x + self.bounds.size.width / 2,
        self.bounds.origin.y + self.bounds.size.height / 2);
}

- (void)setMiddle:(CGPoint)middle
{
    CGPoint oldMiddle = self.middle;
    self.origin = CGPointMake(self.origin.x + oldMiddle.x - middle.x,
                              self.origin.y + oldMiddle.y - middle.y);
}

- (void)pinch:(UIPinchGestureRecognizer*)gesture
{
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateEnded)) {
        self.scale *= gesture.scale;
        gesture.scale = 1;
    }
}

- (void)pan:(UIPanGestureRecognizer*)gesture
{
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateEnded)) {
        CGPoint translation = [gesture translationInView:self];
        self.origin = CGPointMake(
                            self.origin.x + translation.x,
                            self.origin.y + translation.y);
        [gesture setTranslation:CGPointZero inView:self];
    }
}

- (void)tap:(UITapGestureRecognizer*)gesture
{
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateEnded)) {
        if (gesture.numberOfTapsRequired == 2) {
            self.middle = [gesture locationInView:self];
        }
    }
}

- (void)setup
{
    self.scale = DEFAULT_SCALE / self.contentScaleFactor;
    _origin = self.middle;
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [AxesDrawer drawAxesInRect:self.bounds
                 originAtPoint:self.origin
                         scale:self.scale];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 1.0);
    [[UIColor blueColor] setStroke];
    
    CGContextBeginPath(context);
    
    double startX = self.bounds.origin.x - self.origin.x;
    double startY = self.bounds.origin.y +
              [self.dataSource ordinateByGraphView:self andAbscissae:startX];
    CGContextMoveToPoint(context, startX, startY);
    
    double step = 1 / self.scale;
    double end = self.bounds.size.width + self.origin.x;
    for (double x = startX + step; x <= end; x += step) {
        double y = [self.dataSource ordinateByGraphView:self andAbscissae:x];
        CGContextAddLineToPoint(context,
                                x * self.scale + self.origin.x,
                                (y * self.scale) * -1 + self.origin.y);
    }
    CGContextStrokePath(context);
}

@end
