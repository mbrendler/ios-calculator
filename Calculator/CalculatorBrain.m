//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Maik Brendler on 28.06.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import "CalculatorBrain.h"

#pragma mark - Operation

@interface Operation : NSObject

+(BOOL)isOperation:(NSString*)operation;
+(BOOL)needsParentheses:(NSString*)operation other:(NSString*)other;
+(NSString*)description:(NSString*)operation;

@end

@implementation Operation

+(BOOL)isOperation:(NSString *)operation
{
    NSSet *operations = [NSSet setWithObjects:@"π", @"e", @"+/-", @"sin",
                         @"cos", @"log", @"sqrt", @"+", @"*", @"/", @"-", nil];
    return [operations containsObject:operation];
}

+(int)operandCount:(NSString*)operation
{
    //NSSet *noOperands = [NSSet setWithObjects:@"π", @"e", nil];
    NSSet *unary = [NSSet setWithObjects:@"+/-", @"sin", @"cos", @"log",
                    @"sqrt", nil];
    NSSet *binary = [NSSet setWithObjects:@"+", @"*", @"/", @"-", nil];
    if ([unary containsObject:operation]) {
        return 1;
    }
    else if ([binary containsObject:operation]) {
        return 2;
    }
    return 0;
}

+(BOOL)needsParentheses:(NSString*)operation other:(NSString*)other
{
    if ([operation isEqualToString:@"+/-"]) {
        return YES;
    }
    if ([self operandCount:operation] < 2) {
        return NO;
    }
    BOOL hirarchy = (([operation isEqualToString:@"*"] ||
                      [operation isEqualToString:@"/"]) &&
                     [other isEqualToString:@"+"]);
    if ([operation isEqualToString:other] || hirarchy){
        return NO;
    }
    return YES;
}

+(NSString*)description:(NSString*)operation
{
    if ([operation isEqualToString:@"+/-"]) {
        return @"-";
    }
    return operation;
}

@end


@interface CalculatorBrain ()
@property (nonatomic, strong) NSMutableArray *programStack;
@end


@implementation CalculatorBrain

@synthesize programStack = _programStack;

#pragma mark - Calculator

- (NSMutableArray *)programStack {
    if (_programStack == nil) _programStack = [[NSMutableArray alloc] init];
    return _programStack;
}

- (void)clear {
    [self.programStack removeAllObjects];
}

- (void)pushOperand:(double)operand {
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
}

- (void)pushVariable:(NSString*)name {
    [self.programStack addObject:name];
}

- (void) pushOperation:(NSString *)operation {
    [self.programStack addObject:operation];
}

- (id)pop {
    id topOfStack = [self.programStack lastObject];
    if (topOfStack) [self.programStack removeLastObject];
    return topOfStack;
}

- (id)program {
    return [self.programStack copy];
}

# pragma mark - Class methods

+ (double)popOperandOffStack:(NSMutableArray*)stack {
    double result = 0;
    
    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];
    
    if ([topOfStack isKindOfClass:[NSNumber class]]) {
        result = [topOfStack doubleValue];
    } else if ([topOfStack isKindOfClass:[NSString class]] && [Operation isOperation:topOfStack]) {
        NSString *operation = topOfStack;
        if ([operation isEqualToString:@"+"]) {
            result = [self popOperandOffStack:stack] + [self popOperandOffStack:stack];
        } else if ([operation isEqualToString:@"*"]) {
            result = [self popOperandOffStack:stack] * [self popOperandOffStack:stack];
        } else if ([operation isEqualToString:@"-"]) {
            double subtrahend = [self popOperandOffStack:stack];
            result = [self popOperandOffStack:stack] - subtrahend;
        } else if ([operation isEqualToString:@"/"]) {
            double divisor = [self popOperandOffStack:stack];
            if (divisor) {
                result = [self popOperandOffStack:stack] / divisor;
            }
        } else if ([operation isEqualToString:@"sin"]) {
            result = sin([self popOperandOffStack:stack]);
        } else if ([operation isEqualToString:@"cos"]) {
            result = cos([self popOperandOffStack:stack]);
        } else if ([operation isEqualToString:@"sqrt"]) {
            double value = [self popOperandOffStack:stack];
            if (value >= 0) {
                result = sqrt(value);
            }
        } else if ([operation isEqualToString:@"log"]) {
            double value = [self popOperandOffStack:stack];
            if(value) {
                result = log(value);
            }
        } else if ([operation isEqualToString:@"π"]) {
            result = M_PI;
        } else if ([operation isEqualToString:@"e"]) {
            result = M_E;
        } else if ([operation isEqualToString:@"+/-"]) {
            result = [self popOperandOffStack:stack] * -1;
        }
    }
    
    return result;
} 

+ (double)runProgram:(id)program
 usingVariableValues:(NSDictionary *)variableValues {
    NSMutableArray *stack;
    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
        
        if (variableValues) {
            for (int i = 0; i < [stack count]; ++i) {
                id stackElement = [stack objectAtIndex:i];
                NSNumber* variableValue = [variableValues objectForKey:stackElement];
                if (variableValue) {
                    [stack replaceObjectAtIndex:i withObject:variableValue];
                }
            }
        }
    }
    return [self popOperandOffStack:stack];
}

+ (NSSet *)variablesUsedInProgram:(id)program {
    NSMutableSet *result = [NSMutableSet set];
    if ([program isKindOfClass:[NSArray class]]) {
        for (id stackElement in program) {
            if ([stackElement isKindOfClass:[NSString class]]) {
                [result addObject:stackElement];
            }
        }
    }
    if ([result count]) {
        return result;
    }
    return nil;
}

+(NSString*)descriptionOfTopOfStack:(NSMutableArray*)stack
                    lastOperation:(NSString*)lastOperation {
    NSString* result = @"0"; 
    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];
    
    if ([topOfStack isKindOfClass:[NSString class]] && [Operation isOperation:topOfStack]) {
        NSString *operation = topOfStack;
        switch ([Operation operandCount:operation]) {
            case 0:
                result = [Operation description:operation];
                break;
            case 1:
                result = [NSString stringWithFormat:@"%@(%@)", operation,
                          [self descriptionOfTopOfStack:stack
                                          lastOperation:operation]];
                break;
            case 2: {
                NSString *op1 = [self descriptionOfTopOfStack:stack
                                                lastOperation:operation];
                NSString *op2 = [self descriptionOfTopOfStack:stack
                                                lastOperation:operation];
                if ([Operation needsParentheses:operation other:lastOperation]) {
                    result = [NSString stringWithFormat:@"(%@ %@ %@)", op2,
                              operation, op1];
                } else {
                    result = [NSString stringWithFormat:@"%@ %@ %@", op2,
                              operation, op1];
                }
            }
        }
    }
    else if (topOfStack){
        result = [NSString stringWithFormat:@"%@", topOfStack];
    }
    return result;
}

+ (NSString*)descriptionOfProgram:(id)program {
    NSMutableArray *stack;
    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
    }
    NSString *result = [self descriptionOfTopOfStack:stack lastOperation:nil];
    while ([stack count]) {
        result = [result stringByAppendingFormat:@", %@",
                  [self descriptionOfTopOfStack:stack lastOperation:nil]];
    }
    return result;
}

@end
