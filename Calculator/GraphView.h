//
//  GraphView.h
//  Calculator
//
//  Created by Maik Brendler on 13.07.12.
//  Copyright (c) 2012 Private. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GraphView;

@protocol GraphViewDataSource

- (double)ordinateByGraphView:(GraphView*)view andAbscissae:(double)x;

@end

@interface GraphView : UIView

@property (nonatomic, weak) IBOutlet id <GraphViewDataSource> dataSource;
@property (nonatomic) double scale;
@property (nonatomic) CGPoint origin;

@end
